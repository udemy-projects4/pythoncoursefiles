# Dictionary
dictionary = {
    "key": "Value"
}
print("Dict:", dictionary)
dictionary["key"] = "ValueUpdated"
print("Dict:", dictionary)
dictionary["Key"] = "Value2"
print("Dict:", dictionary)
del dictionary["key"]
print("Dict:", dictionary)

# Tuple
tup = (1, 2, 3)
print("Tuple:", tup)


# tuple is immutable
