import datetime
import logging


class Date:
    date = ""
    year = ""
    month = ""
    day = ""

    def __init__(self, date):
        year_inst = month_inst = day_inst = 00
        date_error = "set_date failed due to wrong date format. " + str(date) + " is not in Date[YYYY-MM-DD] Format."
        if date:
            if "-" in date:
                date_split = date.split('-')
                try:
                    month = int(date_split[1])
                    monthcheck = True
                except:
                    logging.error("DATE_MONTH_IN_WRONG_FORMAT:: month must be number and " + date_error)
                    monthcheck = False
                    return
                if len(date_split[0]) == 4 and monthcheck and len(date_split[1]) == 2 and len(date_split[2]) == 2:
                    year_inst = int(date_split[0])
                    month_inst = int(date_split[1])
                    day_inst = int(date_split[2])
                else:
                    logging.error("DATE_LENGTH_CHECK_FAILED:: " + date_error)
                    return
            else:
                logging.error("DATE_SPLIT_FAILED:: " + date_error)
                return
        else:
            print("SET_DATE::", "Generating Today's Date!")
            datetime_now = datetime.datetime.now()
            year_inst = datetime_now.year
            month_inst = datetime_now.month
            day_inst = datetime_now.day
            month_inst = month_inst if len(str(month_inst)) == 2 else "0" + str(month_inst)
            day_inst = day_inst if len(str(day_inst)) == 2 else "0" + str(day_inst)

        date = "{YEAR}-{MONTH}-{DAY}".format(YEAR=year_inst, MONTH=month_inst, DAY=day_inst)
        print("SET_DATE:: Date of Batch is", date)
        self.date = date
        self.year = str(year_inst)
        self.month = str(month_inst)
        self.day = str(day_inst)
        return

    def __str__(self):
        return "Date<object>:\n\tdate: {date}\n\tyear: {year}\n\tmonth: {month}\n\tday: {day}"\
            .format(date=self.date, year=self.year, month=self.month, day=self.day)


class Students:
    __students = []

    def __init__(self, date_of_batch=None):
        self.date_of_batch = Date(date_of_batch)

    def __str__(self):
        return "Students<object>:\n\tdate_of_batch: {date_of_batch}\n\tstudents: {students}"\
            .format(date_of_batch=self.date_of_batch, students=self.__students)

    def students(self):
        return self.__students

    def add(self, name, age, grade):
        self.__students.append({
            "roll_no": self.__generate_roll_number(),
            "name": name,
            "age": age,
            "grade": grade
        })

    def __generate_roll_number(self):
        year_inst = self.date_of_batch.year
        month_inst = self.date_of_batch.month
        last_enrolled_student = self.__students[len(self.__students) - 1] if len(self.__students) > 0 else None
        last_roll_no = int(last_enrolled_student["roll_no"][-4::]) if last_enrolled_student else 0
        roll_number = year_inst[-2::] + month_inst[-2::]

        if len(str(last_roll_no)) == 4:
            roll_number += str(last_roll_no + 1)
        elif len(str(last_roll_no)) == 3:
            roll_number += "0" + str(last_roll_no + 1)
        elif len(str(last_roll_no)) == 2:
            roll_number += "00" + str(last_roll_no + 1)
        elif len(str(last_roll_no)) == 1:
            roll_number += "000" + str(last_roll_no + 1)
        return roll_number

